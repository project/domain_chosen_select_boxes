/**
 * @file
 * Attaches behaviors for the Domain Chosen select module.
 */
(function ($, Drupal) {
    /**
     * @namespace
     */
    Drupal.behaviors.domainChosenSelect = {
        attach: function (context, settings) {
            $('#edit-field-domain-access').chosen({
                width: '100%',
                placeholder_text_multiple: 'Select some options'
            });
            $('#edit-field-domain-admin').chosen({
                width: '100%',
                placeholder_text_multiple: 'Select some options'
            });
        }
    };
})(jQuery, Drupal);
