Domain Chosen Select Boxes
==========================

This module is a Domain Access submodule that makes long, unwieldy check boxes much more user-friendly, using Chosen jQuery plugin.

Requirements
============

The [Domain Access](https://www.drupal.org/project/domain) module.


Usage
=====

By installing and enabling this module, the check boxes for _Domain Access_ and _Domain Admin_ fields will be replaced by a nice _Multiple Select_ widget.

